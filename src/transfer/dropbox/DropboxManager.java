package transfer.dropbox;

import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import javax.swing.JOptionPane;

import org.apache.commons.io.FileUtils;

import com.dropbox.core.DbxAppInfo;
import com.dropbox.core.DbxAuthFinish;
import com.dropbox.core.DbxClient;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.DbxWebAuthNoRedirect;
import com.dropbox.core.DbxWriteMode;

public class DropboxManager {

	// authentication file to store access tokens
	private static final String AUTH_FILE = "auth";
	
	// app key and secret to connect to the Dropbox account
	private String appKey;
	private String appSecret;
	
	// access token for authorised accounts
	private String accessToken;

	// config to make connection request
	private DbxRequestConfig config;
	
	// target Dropbox client
	private DbxClient dropboxClient;
	
	// control for checking successful authentication
	private boolean authenticated;
	
	// list of files to be uploaded
	private List<FileToUpload> filesToUpload;
	
	// create new Dropbox manager and authenticate using app key and secret
	public DropboxManager(String appKey, String appSecret) {
		this.appKey = appKey;
		this.appSecret = appSecret;
		accessToken = "";
		
		config = new DbxRequestConfig("Transfer", Locale.getDefault().toString());
		authenticated = authenticate();
		authenticationSuccessful();
	}
	
	// create new Dropbox manager and authenticate using access token
	public DropboxManager(String accessToken) {
		appKey = "";
		appSecret = "";
		this.accessToken = accessToken;
		
		config = new DbxRequestConfig("Transfer", Locale.getDefault().toString());
		authenticated = authenticateWithAccessToken();
		authenticationSuccessful();
	}
	
	// check if authenticated
	private void authenticationSuccessful() {
		if (authenticated)
	        try {
				System.out.println("Connected to account: " + dropboxClient.getAccountInfo().displayName);
			} catch (Exception e) {
			}
	}
	
	// authenticate with access token
	private boolean authenticateWithAccessToken() {
		dropboxClient = new DbxClient(config, accessToken);
        try {
			dropboxClient.getAccountInfo();
		} catch (Exception e) {
			// DbxException.BadResponse (code 401)
			if (e.getMessage().endsWith("401")) {
				System.out.println("Invalid access token. Re-authentication needed!");
				
				// re-authenticate, ask for app key and secret
				if (appKey.equals(""))
					appKey = JOptionPane.showInputDialog("Enter app key"); 
				if (appSecret.equals(""))
					appSecret = JOptionPane.showInputDialog("Enter app secret");
				
				// remove invalid access token
				Properties authentication = new Properties();
		    	try {
		    		authentication.load(new FileInputStream("auth"));
		    		authentication.remove(appKey);
		            try {
		    			authentication.store(new FileOutputStream(new File(AUTH_FILE)), "");
		    		} catch (Exception e1) {
		    			System.out.println("Error writing authentication file!");
		    			return false;
		    		}
		    	} catch (Exception e1) {
	    			System.out.println("Error reading authentication file!");
	    			return false;
		    	}
				
		    	// authenticate again with key and secret
				return authenticate();
			}

			// other authentication problem: wait for 30 seconds and retry authenticating
        	System.out.println("Error authenticating! (" + e.getMessage() + ")");
        	wait(30);				
        	return authenticateWithAccessToken();
		}
		
		return true;		
	}
	
	// authenticate with app key and secret
	private boolean authenticate() {
		// create app with key and secret
		DbxAppInfo appInfo = new DbxAppInfo(appKey, appSecret);
        
        // get access token for already authenticated accounts
		Properties authentication = new Properties();
    	try {
    		authentication.load(new FileInputStream("auth"));

    		// if authentication file exists, authenticate with access token
    		accessToken = authentication.getProperty(appKey);
        	if (accessToken != null)
        		return authenticateWithAccessToken();
    	} catch (Exception e) {
			System.out.println("Error reading authentication file!");
			return false;
		}

		// authenticate for the first time
        DbxWebAuthNoRedirect webAuth = new DbxWebAuthNoRedirect(config, appInfo);
        String authorizeUrl = webAuth.start();

        // open authentication page via browser
        try {
			Desktop.getDesktop().browse(new URL(authorizeUrl).toURI());
		} catch (Exception e) {
		}
      	
        // wait for user to allow access and enter the authorisation code
        String code = JOptionPane.showInputDialog("Enter authorisation code");        

        // authenticate with entered code
        DbxAuthFinish authFinish = null;
		try {
			authFinish = webAuth.finish(code);
		} catch (Exception e) {
			System.out.println("Authentication failure! (" + e.getMessage() + ")");
			return false;
		}

		// create Dropbox client
		String token = authFinish.accessToken;
        dropboxClient = new DbxClient(config, token);

        // store access token for authorised account
        authentication.put(appKey, token);
        try {
			authentication.store(new FileOutputStream(new File(AUTH_FILE)), "");
		} catch (Exception e) {
			System.out.println("Error writing authentication file!");
			return false;
		}
        
        return true;
	}
	
	// select files to transfer and initiate upload
	public void transfer(String source, String destination, String fileType[]) {
		// only transfer if authenticated
		if (authenticated) {
			filesToUpload = new ArrayList<FileToUpload>();

			// remote file: file type should be url
			String remoteFilename = "";
			if (fileType[0].equals("url")) {
				remoteFilename = source.substring(source.lastIndexOf("/") + 1);
				File remoteFile = new File(remoteFilename);
				// copy file to local filesystem
				try {
					FileUtils.copyURLToFile(new URL(source), remoteFile);
				} catch (Exception e) {
				}
				
				// if copying is successful initiate upload
				if (remoteFile.exists())
					filesToUpload.add(new FileToUpload(remoteFile));
				else
					System.out.println("Source URL does not exist!");
			}
			
			// local file or folder
			else {
				// check if the source folder exists
				File sourceFile = new File(source);
				if (!sourceFile.exists())
					System.out.println("Source file / folder does not exist!");
				else {
					// select single file
					if (sourceFile.isFile())
						filesToUpload.add(new FileToUpload(sourceFile));
					// select all matching files in the folder
					else if (sourceFile.isDirectory()){
						File files[] = sourceFile.listFiles();
						for (int i = 0; i < files.length; i++)
							if (isType(files[i].getName(), fileType))
								filesToUpload.add(new FileToUpload(files[i]));
					}
				}
			}

			// upload files to Dropbox
			uploadFiles(destination);
			System.out.println("Upload finished: " + filesToUpload.size() + " file(s) uploaded");
			
			// remove copy of the remote file from local filesystem
			if (fileType[0].equals("url"))
				new File(remoteFilename).delete();
		}
	}
	
	// upload files to Dropbox
	private void uploadFiles(String destination) {
		// upload each file to Dropbox
		for (int i = 0; i < filesToUpload.size(); i++) {
			if (!filesToUpload.get(i).isUploaded()) {
				File fileToUpload = filesToUpload.get(i).getFile();
				FileInputStream inputStream = null;
				try {
					inputStream = new FileInputStream(fileToUpload);

		        	System.out.println("Uploading file: " + fileToUpload.getName());	        	
		            dropboxClient.uploadFile(destination + fileToUpload.getName(), DbxWriteMode.add(), fileToUpload.length(), inputStream);
		            filesToUpload.get(i).setUploaded(true);
		        } catch (Exception e) {
		        	System.out.println("Error uploading file! (" + e.getMessage() + ")");
		        
		        	// wait for 30 seconds and retry uploading the remaining files
		        	wait(30);	
		        	uploadFiles(destination);
		        }
				
				// close input stream after upload is completed
				try {
					inputStream.close();
				} catch (Exception e) {
				}
			}
		}
	}

	// check whether the file is of given type
	private boolean isType(String filename, String fileType[]) {
		for (int i = 0; i < fileType.length; i++)
			if (filename.toLowerCase().endsWith(fileType[i].toLowerCase()))
				return true;
		
		return false;
	}
	
	// sleep for certain amount of time
	private void wait(int duration) {
		int step = 10;
		System.out.println("Retrying in " + duration + " seconds");

		if (duration < step)
			step = duration;
		try {
			Thread.sleep(step * 1000);
		} catch (Exception e) {
		}
		
		int remaining = duration - step;
		if (remaining > 0)
			wait(remaining);
	}
	
}