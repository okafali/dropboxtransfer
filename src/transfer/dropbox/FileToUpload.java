package transfer.dropbox;

import java.io.File;

public class FileToUpload {

	private File file;
	private boolean uploaded;

	public FileToUpload(File file) {
		this.file = file;
		uploaded = false;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public boolean isUploaded() {
		return uploaded;
	}

	public void setUploaded(boolean uploaded) {
		this.uploaded = uploaded;
	}
	
}