package transfer.dropbox;

import java.io.FileInputStream;
import java.util.Properties;

public class Start {

	// config file to read parameters
	private static final String CONFIG_FILE = "config";
	
	public static void main(String[] args) {
		// program parameters
    	String appKey = "";
    	String appSecret = "";
    	String accessToken = "";
    	String sourcePath = "";
    	String destinationPath = "";
    	String fileType = "";
    	
		// get parameters from command line
    	
    	// [accessToken] [source] [destination] [fileType]
		if (args.length == 4) {
			accessToken = args[0];
			sourcePath = args[1];
			destinationPath = args[2];
			fileType = args[3];
		}
    	
		// [appKey] [appSecret] [source] [destination] [fileType]
		else if (args.length == 5) {
			appKey = args[0];
			appSecret = args[1];
			sourcePath = args[2];
			destinationPath = args[3];
			fileType = args[4];
		}
		
		// read config file to get parameters
		else {
			Properties properties = new Properties();
	    	try {
				properties.load(new FileInputStream(CONFIG_FILE));
			} catch (Exception e) {
				System.out.println("Error reading config file!");
				System.exit(-1);
			}
	    	
	    	appKey = properties.getProperty("appKey");
	    	appSecret = properties.getProperty("appSecret");
	    	accessToken = properties.getProperty("accessToken");
	    	sourcePath = properties.getProperty("sourcePath");
	    	destinationPath = properties.getProperty("destinationPath");
	    	fileType = properties.getProperty("fileType");
		}

		// extract file types
		String fileTypes[] = fileType.split(",");
    	
    	// create new DroxboxTransfer object
    	DropboxManager dropbox = null;
    	if (!accessToken.equals(""))
    		// access token is given
    		dropbox = new DropboxManager(accessToken);
    	else	
    		// no access token is given
    		dropbox = new DropboxManager(appKey, appSecret);
   		
    	// transfer files 
    	dropbox.transfer(sourcePath, destinationPath, fileTypes);
	}

}